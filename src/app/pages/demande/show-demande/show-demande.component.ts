import { Component, OnInit } from '@angular/core';
import { DemandeService } from '../demande.service';
import { DemandeModel } from '../demande.model';

@Component({
  selector: 'ngx-show-demande',
  templateUrl: './show-demande.component.html',
  styleUrls: ['./show-demande.component.scss']
})
export class ShowDemandeComponent implements OnInit {

  demande: DemandeModel;

  constructor(private service: DemandeService) { }

  ngOnInit() {
    this.demande = new DemandeModel();
    let id = localStorage.getItem('idDemande');
    this.service.getDemandesById(+id).subscribe(
      data => { this.demande = data;
         console.log(data); },
     error => {
         console.log('erreur');
     });
  }
}