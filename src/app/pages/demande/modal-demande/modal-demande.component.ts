import { Component, OnInit } from '@angular/core';
import { DemandeModel } from '../demande.model';
import { DemandeService } from '../demande.service';
import { NbWindowRef } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-modal-demande',
  templateUrl: './modal-demande.component.html',
  styleUrls: ['./modal-demande.component.scss']
})
export class ModalDemandeComponent implements OnInit {
  demande: DemandeModel;
  A: string;
  
  ngOnInit(): void {
    let e = localStorage.getItem('e');
    this.demande = new DemandeModel();
    if (e === '0' ) {
      this.A = 'Ajouter';
    }
    if (e === '1') {
     this.A = 'Modifier';
     let id = localStorage.getItem('idDemande');
          console.log(id);
     this.service.getDemandesById(+id).subscribe(
       data => { this.demande = data;
          console.log(data); },
      error => {
          console.log('erreur');
      });
    }
  }

  constructor(public windowRef: NbWindowRef,
              private service: DemandeService,
     private router: Router) { }

  close() {
    this.windowRef.close();
    this.router.navigate(['/pages/demande']);
  }

  onAddM() {
    let e = localStorage.getItem('e');
    if (e === '0') {
      this.service.addDemandes(this.demande)
        .subscribe(data => {
            localStorage.removeItem('e');
            localStorage.removeItem('idDemande');
            this.windowRef.close();
            this.router.navigate(['/pages/refreshDemande']);
          },
          error => {
            console.log('error');
          });
    }
    if (e === '1') {
      this.service.updateDemandes(this.demande).subscribe(
        data => {
        localStorage.removeItem('e');
        localStorage.removeItem('idDemande');
        this.windowRef.close();
        
        this.router.navigate(['/pages/refreshDemande']);
        },
        error => {
          console.log('error');
        });
    }
  }
}