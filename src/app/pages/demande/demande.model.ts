export class DemandeModel{
    id: number;
    nombreCartons: number;
    poidsCartons: number;
    longueur: number;
    largeur: number;
    hauteur: number;
    poidsTotal:number;
    nature: string;
    dateEnvoi: Date;
    idpersonne: number;
    description: string;
}