import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { DemandeModel } from "./demande.model";
import { PagesComponent } from "../pages.component";

@Injectable({
    providedIn: 'root',
  })
  export class DemandeService {
    url = PagesComponent.urlConfig + '/demandes';
    constructor(protected httpClient: HttpClient) { }
    getAllDemandes() {
      return this.httpClient.get<DemandeModel[]>(this.url);
    }
    getDemandesById(id: number) {
      return this.httpClient.get<DemandeModel>(this.url + '/' + id);
    }
    addDemandes(demandeModel: DemandeModel) {
      return this.httpClient.post(this.url, demandeModel);
    }
    updateDemandes(demandeModel: DemandeModel) {
      return this.httpClient.put(this.url + '/' + demandeModel.id, demandeModel);
    }
    deleteEmployes(id: Number) {
      return this.httpClient.delete(this.url + '/' + id);
    }
}