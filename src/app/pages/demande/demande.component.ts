import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ModalDemandeComponent } from './modal-demande/modal-demande.component';
import { ShowDemandeComponent } from './show-demande/show-demande.component';
import { DemandeService } from './demande.service';
import { NbWindowService } from '@nebular/theme';

@Component({
  selector: 'ngx-demande',
  templateUrl: './demande.component.html',
  styleUrls: ['./demande.component.scss']
})
export class DemandeComponent implements OnInit {
  source: any;

  constructor(private service: DemandeService,
              private windowService: NbWindowService) {
  }

  ngOnInit(): void {
    this.service.getAllDemandes().subscribe(
      data => { this.source = data; },
      error => { console.log("error"); });
  }

  settings = {
    actions: {
      add: false,
      edit: false,
      position: 'right',
      custom: [
        {
          name: 'showAction',
          title: '<i class="nb-sunny" title="Show"></i>',
        },
        {
          name: 'editAction',
          title: '<i class="nb-edit" title="Edit"></i>',
        },
      ],
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'Num demande',
        type: 'number',
      },
      dateEnvoi: {
        title: "Date d'Envoi",
        type: 'date',
      },
      description: {
        title: 'Description',
        type: 'textarea',
        editor: {type: 'textarea'},
      },
    },
  };
  
  openWindow() {
    localStorage.removeItem('e');
    localStorage.removeItem('idDemande');
    localStorage.setItem('e', '0');
    this.windowService.open(ModalDemandeComponent, {title: 'Ajouter une demande'},
      );
  }

  onDeleteConfirm(event): void {
    if (window.confirm(`Vous etes sure de supprimer cet demande?`)) {
      event.confirm.resolve(this.service.deleteEmployes(event.data.id).subscribe(
        data => {
          this.source.filter(p => p !== event.data);
        }),
      );
    } else {
      event.confirm.reject();
    }
  }

  onCustom(event): any {
    if (event.action === 'editAction') {
      localStorage.removeItem('e');
      localStorage.removeItem('idDemande');
      localStorage.setItem('idDemande' , event.data.id);
      localStorage.setItem('e', '1');
      this.windowService.open(ModalDemandeComponent, {title: 'Modifier cet Demande'});
    }
    if (event.action === 'showAction') {
      localStorage.removeItem('e');
      localStorage.removeItem('idDemande');
      localStorage.setItem('idDemande' , event.data.id);
      this.windowService.open(ShowDemandeComponent, {title: 'Afficher les informations de cet Demande'});
      console.log('show');
    }
  }
}
