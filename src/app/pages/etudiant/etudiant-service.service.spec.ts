import { TestBed } from '@angular/core/testing';

import { EtudiantServiceService } from './etudiant-service.service';

describe('EtudiantServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EtudiantServiceService = TestBed.get(EtudiantServiceService);
    expect(service).toBeTruthy();
  });
});
