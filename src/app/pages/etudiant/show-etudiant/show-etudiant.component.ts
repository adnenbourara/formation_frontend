import { Component, OnInit } from '@angular/core';
import { EtudiantServiceService}from '../etudiant-service.service';
import {NbWindowRef} from '@nebular/theme';
import { Etudiant } from '../etudiant';
@Component({
  selector: 'ngx-show-etudiant',
  templateUrl: './show-etudiant.component.html',
  styleUrls: ['./show-etudiant.component.scss']
})
export class ShowEtudiantComponent implements OnInit {
  etudiant:Etudiant;

  constructor(private service:EtudiantServiceService  , private windowRef: NbWindowRef ) { }

  ngOnInit() {

    this.etudiant = new Etudiant();
    let id = localStorage.getItem('idEtudiant');
    this.service.getById(+id).subscribe(
      data => { this.etudiant = data;
         console.log(data); },
     error => {
         console.log('erreur');
     });
  }
  fermer()
  {
    this.windowRef.close();
  }
  }


