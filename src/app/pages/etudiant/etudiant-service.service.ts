import { Injectable } from '@angular/core';
import {Etudiant} from './etudiant';
import { HttpClient } from "@angular/common/http";
import { PagesComponent } from "../pages.component";

@Injectable({
  providedIn: 'root'
})
export class EtudiantServiceService {
  url = PagesComponent.urlConfig+'/etudiants'
  url2= PagesComponent.urlConfig+'/classes'
  urlEtudiant='/etudiants'

  constructor(protected httpclient : HttpClient) { }
  getAllEtudiant(){
    return this.httpclient.get<Etudiant[]>(this.url);
  }
  addEtudiant(etudiant:Etudiant ){
    return this.httpclient.post(this.url2+'/'+etudiant.classe.id+'/etudiants',etudiant);
  }
  getById(id:number){
    return this.httpclient.get<Etudiant>(this.url+'/'+id);
  }
  editEtudiant(etudiant:Etudiant){
    return this.httpclient.put(this.url+'/'+etudiant.id,etudiant);
  }
  deleteEtudiant(id:number){
    return this.httpclient.delete(this.url+'/'+id);
  }
  
}
