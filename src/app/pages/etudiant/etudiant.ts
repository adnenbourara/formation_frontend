import { Classe } from "../classe/classe";

export class Etudiant {
    
    id:number;
    nom :String;
    prenom :String;
    email :String;
    date :Date;
    classe:Classe;
}
