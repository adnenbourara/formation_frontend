import { Component, OnInit } from '@angular/core';
import {Etudiant} from '../etudiant' ;
import {EtudiantServiceService}from '../etudiant-service.service';
import { Router } from '@angular/router';
import {  NbWindowRef, NbGlobalLogicalPosition, NbToastrService, NbGlobalPosition, NbNativeDateService } from '@nebular/theme';
import { ClasseServiceService } from '../../classe/classe-service.service';
import { ToastService } from '../../toast.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'ngx-modal-etudiant',
  templateUrl: './modal-etudiant.component.html',
  styleUrls: ['./modal-etudiant.component.scss']
})
export class ModalEtudiantComponent implements OnInit {
  myForm
  
  
  etudiant:Etudiant;
  A: string;
 classes=[];


  constructor(private service : EtudiantServiceService , 
    private router : Router , 
    public windowRef: NbWindowRef,
    private serviceClasse : ClasseServiceService,
    private toastrService: ToastService,
   

) { }

  ngOnInit() { 
    




    this.serviceClasse.getallClasse().subscribe(
      data =>{ this.classes= data; 
        console.log(data)
      }

    )

  

    let e = localStorage.getItem('e');
  this.etudiant = new Etudiant();
  if (e === '0' ) {
    this.A = 'Ajouter';
  }
  if (e === '1') {
   this.A = 'Modifier';
   let id = localStorage.getItem('idEtudiant');
        console.log(id);
   this.service.getById(+id).subscribe(
     data => { 
     
      this.etudiant = data;
      this.etudiant.date = new Date(data.date); 
     },
    error => {
        console.log('erreur');
    });
  }

  }

  addE(){
    if(this.etudiant.date > new Date)
    {
    console.log(this.etudiant)
    let e = localStorage.getItem('e');
    if (e === '0') {
      this.service.addEtudiant(this.etudiant)
        .subscribe(data => {
            localStorage.removeItem('e');
            localStorage.removeItem('idEtudiant');
            this.windowRef.close();
            this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
          this.router.navigate(['/pages/etudiant']));
          this.toastrService.showToast(NbToastStatus.SUCCESS,'Etudiant ajouté','');
          },
          error => {
            console.log('error');
          })}

    if (e==='1'){
this.service.editEtudiant(this.etudiant).subscribe(data => {  localStorage.removeItem('e');
localStorage.removeItem('idEtudiant');
this.windowRef.close();
this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
this.router.navigate(['/pages/etudiant']));
this.toastrService.showToast(NbToastStatus.INFO,'Etudiant modifié','');


},
error => {
  console.log('error');
}
  )

    }
  }
  else
  this.toastrService.showToast(NbToastStatus.WARNING,'Date eronnée','choisissez une date valide');


}
  fermer()
  {
    this.windowRef.close();
  }
 
}
