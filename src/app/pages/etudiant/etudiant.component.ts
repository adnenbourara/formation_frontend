import { Component, OnInit } from '@angular/core';
import {EtudiantServiceService} from './etudiant-service.service';
import {ModalEtudiantComponent} from './modal-etudiant/modal-etudiant.component' ;
import {ShowEtudiantComponent} from './show-etudiant/show-etudiant.component';
// import {ShowEtudiantComponent} from  ;
import {NbWindowService }from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { ToastService } from '../toast.service';
@Component({
  selector: 'ngx-etudiant',
  templateUrl: './etudiant.component.html',
  styleUrls: ['./etudiant.component.scss']
})
export class EtudiantComponent implements OnInit {
  source :any; 

  constructor(private service : EtudiantServiceService , 
    private windowService: NbWindowService,
    private toastrService: ToastService) { }

  ngOnInit() {
    this.service.getAllEtudiant().subscribe (

      data => { this.source = data; 
      console.log(data)},
      error => { console.log("error"); });

  }

  settings = {
    actions: {
      add: false,
      edit: false,
      position: 'right',
      custom: [
        {
          name: 'showAction',
          title: '<i class="nb-sunny" title="Show"></i>',
        },
        {
          name: 'editAction',
          title: '<i class="nb-edit" title="Edit"></i>',
        },
      ],
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      nom: {
        title: 'nom',
        type: 'text',
      },
      prenom: {
        title: "Prenom",
        type: 'text',
      },
      email: {
        title: "email",
        type: 'text',
      },
      date: {
        title: "date",
        type: 'date',
      },
      classe: {
        title: "label classe",
        type: 'number',
        valuePrepareFunction: (cel) => 
          { return  cel.label
          }
      },
    },

  };
  openWindow() {
    localStorage.removeItem('e');
    localStorage.removeItem('idEtudiant');
    localStorage.setItem('e', '0');
    this.windowService.open(ModalEtudiantComponent, {title: 'Ajouter un etudiant'},
      );
  }
  onCostum(event) :any {
    if (event.action === 'editAction') {
   localStorage.removeItem('e');
   localStorage.removeItem('idEtudiant');
   localStorage.setItem('idEtudiant' , event.data.id);
   localStorage.setItem('e', '1');
   this.windowService.open(ModalEtudiantComponent, {title: 'Modifier cette Classe'});
   }
   if (event.action === 'showAction') {
     localStorage.removeItem('e');
     localStorage.removeItem('idEtudiant');
     localStorage.setItem('idEtudiant' , event.data.id);
     this.windowService.open(ShowEtudiantComponent, {title: 'Afficher les informations de cet Etudiant'});
     console.log('show');
   }

  
}
onDeleteConfirm(event): void {
  if (window.confirm(`Vous etes sure de supprimer cet etudiant ?`)) {
    event.confirm.resolve(this.service.deleteEtudiant(event.data.id).subscribe(
      data => {
        this.source.filter(p => p !== event.data);
        this.toastrService.showToast(NbToastStatus.WARNING,'Etudiant supprimé','');
        
      }),
    );
  } else {
    event.confirm.reject();
  }
}
}