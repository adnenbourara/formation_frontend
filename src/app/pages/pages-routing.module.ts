import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import {ParametreComponent} from './parametre/parametre.component';
import { DemandeComponent } from './demande/demande.component';
import { ClasseComponent } from './classe/classe.component';
import { EtudiantComponent } from './etudiant/etudiant.component';


const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: ECommerceComponent,
  }, {
      path: 'parametre',
      component: ParametreComponent,
    },
    {
      path: 'demande',
      component: DemandeComponent,
    }, 
    {
      path: 'classe',
      component: ClasseComponent,
    },
    {
      path: 'etudiant',
      component: EtudiantComponent,
    },
    {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
    {
    path: '**',
    component: NotFoundComponent,
  }, {
      path: 'parametre',
      component: ParametreComponent,
    }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
