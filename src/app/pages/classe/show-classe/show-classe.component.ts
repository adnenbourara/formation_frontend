import { Component, OnInit } from '@angular/core';
import {Classe}from '../classe';
import {ClasseServiceService} from '../classe-service.service' ;
import {  NbWindowRef } from '@nebular/theme';

@Component({
  selector: 'ngx-show-classe',
  templateUrl: './show-classe.component.html',
  styleUrls: ['./show-classe.component.scss']
})
export class ShowClasseComponent implements OnInit {

  classe: Classe;

  constructor(private service: ClasseServiceService , private windowRef: NbWindowRef ) { }

  ngOnInit() {
    this.classe = new Classe();
    let id = localStorage.getItem('idClasse');
    this.service.getClassById(+id).subscribe(
      data => { this.classe = data;
         console.log(data); },
     error => {
         console.log('erreur');
     });
  }
  fermer()
  {
    this.windowRef.close();
  }
}