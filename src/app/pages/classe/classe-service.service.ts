import { Injectable } from '@angular/core';
import {Classe} from "./classe"
import { HttpClient } from "@angular/common/http";
import { PagesComponent } from "../pages.component";

@Injectable({
  providedIn: 'root'
})
export class ClasseServiceService {
  url = PagesComponent.urlConfig+'/classes'

  constructor(  protected httpclient : HttpClient) {}

  getallClasse(){
    return this.httpclient.get<Classe[]>(this.url);

  }

getClassById(id:Number)
{
  return this.httpclient.get<Classe>(this.url+'/'+id);
}

deleteClassById(id:number)
{  return this.httpclient.delete(this.url+'/'+id);
}

addClasse(classe:Classe)
{
  return this.httpclient.post(this.url,classe);
}

updateClasse(classe:Classe)
{
  return this.httpclient.put(this.url+'/'+classe.id,classe)
}







  


}
