import { TestBed } from '@angular/core/testing';

import { ClasseServiceService } from './classe-service.service';

describe('ClasseServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClasseServiceService = TestBed.get(ClasseServiceService);
    expect(service).toBeTruthy();
  });
});
