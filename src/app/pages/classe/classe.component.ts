import { Component, OnInit, Input, EventEmitter, Output  } from '@angular/core';

import {ClasseServiceService} from "./classe-service.service";
import { ModalClasseComponent } from './modal-classe/modal-classe.component';
import { ShowClasseComponent} from './show-classe/show-classe.component';
import { NbWindowService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { ToastService } from '../toast.service';



@Component({
  selector: 'ngx-classe',
  templateUrl: './classe.component.html',
  styleUrls: ['./classe.component.scss']
})
export class ClasseComponent implements OnInit {

source :any; 


  constructor( private service : ClasseServiceService , 
    private windowService: NbWindowService,
    private toastrService:ToastService,

  ) { }

  ngOnInit() {

this.service.getallClasse().subscribe (

  data => { this.source = data; },
  error => { console.log("error"); });


}

settings = {
  actions: {
    add: false,
    edit: false,
    position: 'right',
    custom: [
      {
        name: 'showAction',
        title: '<i class="nb-sunny" title="Show"></i>',
      },
      {
        name: 'editAction',
        title: '<i class="nb-edit" title="Edit"></i>',
      },
    ],
  },
  delete: {
    deleteButtonContent: '<i class="nb-trash"></i>',
    confirmDelete: true,
  },
  columns: {
    fullname: {
      title: 'Full name',
      type: 'text',
    },
    label: {
      title: "label",
      type: 'text',
    },
  
  },
};



openWindow() {
  localStorage.removeItem('e');
  localStorage.removeItem('idClasse');
  localStorage.setItem('e', '0');
  this.windowService.open(ModalClasseComponent, {title: 'Ajouter une classe'},
    );
}
onCostum(event) :any {
   if (event.action === 'editAction') {
  localStorage.removeItem('e');
  localStorage.removeItem('idClasse');
  localStorage.setItem('idClasse' , event.data.id);
  localStorage.setItem('e', '1');
  this.windowService.open(ModalClasseComponent, {title: 'Modifier cette Classe'});
  }
  if (event.action === 'showAction') {
    localStorage.removeItem('e');
    localStorage.removeItem('idClasse');
    localStorage.setItem('idClasse' , event.data.id);
    this.windowService.open(ShowClasseComponent, {title: 'Afficher les informations de cette Classe'});
    console.log('show');
  }


}
onDeleteConfirm(event): void {
  if (window.confirm(`Vous etes sure de supprimer cet demande?`)) {
    event.confirm.resolve(this.service.deleteClassById(event.data.id).subscribe(
      data => {
        this.source.filter(p => p !== event.data);
        this.toastrService.showToast(NbToastStatus.WARNING,'Classe supprimé','');
      }),
    );
  } else {
    event.confirm.reject();
  }
}
  }



