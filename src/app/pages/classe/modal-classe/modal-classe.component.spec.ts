import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalClasseComponent } from './modal-classe.component';

describe('ModalClasseComponent', () => {
  let component: ModalClasseComponent;
  let fixture: ComponentFixture<ModalClasseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalClasseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalClasseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
