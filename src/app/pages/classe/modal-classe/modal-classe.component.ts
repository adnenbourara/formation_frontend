import { Component, OnInit } from '@angular/core';
import {Classe} from '../classe';
import {ClasseServiceService} from '../classe-service.service';
import { Router } from '@angular/router';
import {  NbWindowRef } from '@nebular/theme';
import { ShowClasseComponent } from '../../classe/show-classe/show-classe.component';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { ToastService } from '../../toast.service';

@Component({
  selector: 'ngx-modal-classe',
  templateUrl: './modal-classe.component.html',
  styleUrls: ['./modal-classe.component.scss']
})
export class ModalClasseComponent implements OnInit {
classe :Classe;
A: string;
  constructor(
    private service : ClasseServiceService , private router : Router , 
    public windowRef: NbWindowRef,
    private toastrService:ToastService
  ) {}
  ngOnInit() {
    let e = localStorage.getItem('e');
    this.classe = new Classe();
    if (e === '0' ) {
      this.A = 'Ajouter';
    }
    if (e === '1') {
     this.A = 'Modifier';
     let id = localStorage.getItem('idClasse');
          console.log(id);
     this.service.getClassById(+id).subscribe(
       data => { this.classe = data;
          console.log(data); },
      error => {
          console.log('erreur');
      });
    }
  }

  

  
   
    
    onAddC() {


      let e = localStorage.getItem('e');
      if (e === '0') {

        this.service.addClasse(this.classe)
          .subscribe(data => {


              localStorage.removeItem('e');
              localStorage.removeItem('idClasse');
              this.windowRef.close();
              this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
            this.router.navigate(['/pages/classe']));
            this.toastrService.showToast(NbToastStatus.SUCCESS,'Classe ajouté','success');
            
            },
            error => {
              console.log('error');
            })}
        if( e === '1') {
          this.service.updateClasse(this.classe)
            .subscribe(data => {
                localStorage.removeItem('e');
                localStorage.removeItem('idClasse');
                this.windowRef.close();
                this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
              this.router.navigate(['/pages/classe']));
              this.toastrService.showToast(NbToastStatus.INFO,'Classe modifié','success');
              },
              error => {
                console.log('error');
              });}

  
 
}
fermer()  
{
  this.windowRef.close();
}
  }
  

  

