import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from "ng2-smart-table";
import { PagesComponent } from './pages.component';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import {ParametreModule} from './parametre/parametre.module';
import { DemandeComponent } from './demande/demande.component';
import { ModalDemandeComponent } from './demande/modal-demande/modal-demande.component';
import { ShowDemandeComponent } from './demande/show-demande/show-demande.component';
import { RefreshDemandeComponent } from './demande/refresh-demande/refresh-demande.component';
import { NbDialogModule, NbWindowModule } from '@nebular/theme';
import { EtudiantComponent } from './etudiant/etudiant.component';
import { ClasseComponent } from './classe/classe.component';
import { ModalClasseComponent } from './classe/modal-classe/modal-classe.component';
import { ShowClasseComponent } from './classe/show-classe/show-classe.component';
import { ModalEtudiantComponent } from './etudiant/modal-etudiant/modal-etudiant.component';
import { ShowEtudiantComponent } from './etudiant/show-etudiant/show-etudiant.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';


const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    ECommerceModule,
    Ng2SmartTableModule,
    NbDialogModule.forChild(),
    NbWindowModule.forChild(),
    MiscellaneousModule,
    ParametreModule,
    NgSelectModule,
    CommonModule,
     
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    DemandeComponent,
    ModalDemandeComponent,
    ShowDemandeComponent,
    RefreshDemandeComponent,
    EtudiantComponent,
    ClasseComponent,
    ModalClasseComponent,
    ShowClasseComponent,
    ModalEtudiantComponent,
    ShowEtudiantComponent,

  ],
  entryComponents:[
   ModalClasseComponent,
    ModalDemandeComponent,
    ShowDemandeComponent,
    ShowClasseComponent,
    ModalDemandeComponent,
    ShowEtudiantComponent,
    ModalEtudiantComponent,


  ]
})
export class PagesModule {
}
