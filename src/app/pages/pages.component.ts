import { Component } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent {
  public static urlConfig = 'http://localhost:9095';
  menu = MENU_ITEMS;
  constructor(private translate: TranslateService){

  }
  tarnslate(){
  MENU_ITEMS.push(this.translate.instant('test.classefelmenu'))
  }
}
